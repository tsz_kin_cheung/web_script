from selenium import webdriver
import time
import os
import requests


# keyword = 'tuna+sushi'
url = 'https://www.google.com/search?q=salmon+sushi&sxsrf=ALeKk00ep80hmn8TAuFwYsqwNo8XUW7U_w:1625215087367&source=lnms&tbm=isch&sa=X&ved=2ahUKEwj13dz__cPxAhUNWCsKHdquDYkQ_AUoAXoECAIQAw&biw=1197&bih=1153'


class Crawler_google_images:
 
    def __init__(self):
        self.url = url


    def init_browser(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--disable-infobars")
        browser = webdriver.Chrome("./chromedriver.exe")
        browser.get(self.url)
        browser.maximize_window()
        return browser


    def download_images(self, browser, num=100):

        picpath = 'C:/Users/85251/Desktop/simple'
        if not os.path.exists(picpath): os.makedirs(picpath)

        count = 0  
        pos = 0
        # print(num)

        while (True):
            try:
 
                js = 'var q=document.documentElement.scrollTop=' + str(pos)
                pos += 500
                browser.execute_script(js)
                time.sleep(1)
                img_elements = browser.find_elements_by_xpath('//a[@class="wXeWr islib nfEiy"]')
                try:
                    for img_element in img_elements:

                        img_element.click()
                        time.sleep(0.5)
                        try:
 
                            balabalas = browser.find_elements_by_xpath('//img[@class="n3VNCb"]')

                            if (balabalas):
                                for balabala in balabalas:
                                    try:
                                        src = balabala.get_attribute('src')
                                        try:
                                            if src.startswith('http') and not src.startswith(
                                                    'https://encrypted-tbn0.gstatic.com'):
                                                print('Found ' + str(count) + ' st image url')
                                                self.save_img(count, src, picpath)
                                                count += 1
                                                if (count >= num):
                                                    return "stop"
                                        except:
                                            print('get image fail')
                                    except:
                                        print('get image fail')
                        except:
                            print('get image fail in get section')

                    #back
                    browser.back()
                    time.sleep(0.3)
                except:
                    print('get page fail')
            except:
                print("can't get page")

    def save_img(self, count, img_src, picpath):
        filename = picpath + '/' + 'mixed_new_35_'+ str(count) + '.jpg'
        r = requests.get(img_src)
        with open(filename, 'wb') as f:
            f.write(r.content)
        f.close()

    def run(self):
        self.__init__()
        browser = self.init_browser()
        self.download_images(browser, 20)  
        browser.close()
        print("############finish################")


if __name__ == '__main__':
    craw = Crawler_google_images()
    craw.run()
